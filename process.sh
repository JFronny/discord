#!/bin/bash
file="$*"
fileout="$file.build"
prefix="@import "
if [ -f "$fileout" ]
then
    rm -f "$fileout"
fi

while read line
do
    if [[ "$line" == "$prefix"* ]]
    then
        path="${line#"$prefix"}"
        if [ -f "$path" ]
        then
            cat "$path" >> "$fileout"
        else
            curl "$path" >> "$fileout"
        fi
    else
        echo "$line" >> "$fileout"
    fi
done < "$file"
