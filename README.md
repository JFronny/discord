# Discord mods
This repository contains userscripts which modify the browser-based discord a bit

## Included scripts
- [a custom theme](https://jfronny.gitlab.io/userscripts/themes.user.css) based on CutTheTheme, RadialStatus and HorizontalServerList. Requires the JetBrainsMono Nerd Font
- [a mute button](https://jfronny.gitlab.io/userscripts/muteButton.user.js) for text channels

## Old features that I used before but that aren't available here:
- [Corrupt text in messages](https://github.com/rauenzi/BetterDiscordAddons/tree/master/Plugins/BetterFormattingRedux)
- Allow zooming in the integrated image viewer, other stuff can be done through standard browser features -> [Image Tools](https://github.com/userXinos/image-tools) or [Image Utilities](https://github.com/mwittrien/BetterDiscordAddons/tree/master/Plugins/ImageUtilities) (another feature of these that is not possible by default in the browser is displaying additional details for the images, I don't need that though)
- [Double click messages to edit them](https://github.com/powercord-org/powercord/blob/v2/src/Powercord/plugins/pc-clickableEdits/index.js)
- [Blur NSFW content](https://github.com/E-boi/Powercord-BlurNSFW)
- [Show a list of members who reacted next to the reactions](https://github.com/jaimeadf/who-reacted)

# Other stuff
- [Rickroll remover](https://jfronny.gitlab.io/userscripts/rickless.user.js)
