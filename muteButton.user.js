// ==UserScript==
// @name        Mute Button for Discord 
// @namespace   https://jfronny.gitlab.io
// @match       https://discord.com/*
// @match       https://canary.discord.com/*
// @version     1.1
// @author      JFronny
// @description Deprecated
// @require     https://code.jquery.com/jquery-3.6.0.min.js
// ==/UserScript==

const token = window.localStorage.token.replace(/['"]+/g, '');
const svgMute = `<svg class="actionIcon-PgcMM2" aria-hidden="true" width="16" height="16" viewBox="0 0 24 24"><path fill="currentColor" fill-rule="evenodd" clip-rule="evenodd" d="M18 9V14C18 15.657 19.344 17 21 17V18H3V17C4.656 17 6 15.657 6 14V9C6 5.686 8.686 3 12 3C15.314 3 18 5.686 18 9ZM11.9999 21C10.5239 21 9.24793 20.19 8.55493 19H15.4449C14.7519 20.19 13.4759 21 11.9999 21Z"></path></svg>`;

function isMuted(contentElement) {
  return $(contentElement).parents().hasClass('modeMuted-onO3r-');
}

function buildButton(contentElement, mainContent) {
  const children = $(contentElement).find(".children-3rEycc");
  children.find(".jfdiscord-mute").remove();
  const html = `<div class="iconItem-F7GRxr iconBase-3LOlfs jfdiscord-mute" aria-label="Kanal stummschalten" tabindex="0" role="button">` + svgMute + "</div>";
  const button = children.prepend(html).children()[0];
  button.onclick = function(){
    const sp = mainContent.getAttribute("href").split("/");
    const c = {
      "credentials": "include",
      "headers": {
          "Content-Type": "application/json",
          "Authorization": token
      },
      "body": "{\"channel_overrides\":{\"" + sp[3] + "\":{\"muted\":" + (!isMuted(contentElement)) + ",\"mute_config\":{\"selected_time_window\":-1,\"end_time\":null}}}}",
      "method": "PATCH",
      "mode": "cors"
    };
    fetch("https://discord.com/api/v9/users/@me/guilds/" + sp[2] + "/settings", c)
      .then(function (response) {
      buildButton(contentElement, mainContent);
    });
  }
}



$(document).on("DOMSubtreeModified", function() {
  $("#channels").on('DOMSubtreeModified', function(){
    $(".content-1x5b-n[data-touched!='true']").each(function(){
      $(this).attr("data-touched", "true");
      const mainContent = $(this).find(".mainContent-u_9PKf")[0];
      if (mainContent.hasAttribute("href")) {
        buildButton(this, mainContent);
      }
    });
  });
});
